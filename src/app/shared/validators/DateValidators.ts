import { AbstractControl } from '@angular/forms';
import * as moment from 'moment';

export class DateValidators {
  static past(control: AbstractControl) {
    if (moment(control.value).isValid && moment().isBefore(control.value)) {
      return { past: true };
    }
    return null;
  }
}
