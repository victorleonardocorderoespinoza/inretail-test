import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomerService } from '../customer-service/customer.service';
import { DateValidators } from 'src/app/shared/validators/DateValidators';

@Component({
  selector: 'in-customer-new',
  templateUrl: './customer-new.component.html'
})
export class CustomerNewComponent implements OnInit {

  submitted = false;
  customerForm = this.formBuilder.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    dob: ['', [Validators.required, DateValidators.past]]
  });

  constructor(private formBuilder: FormBuilder, private customerService: CustomerService, private router: Router) { }

  ngOnInit() {
  }

  isInvalidControl(control: string) {
    return this.customerForm.get(control).invalid && this.submitted;
  }

  onSubmit() {
    this.submitted = true;
    if (this.customerForm.valid) {
      const customer = this.customerForm.value;
      this.customerService.add(customer);
      this.router.navigate(['clientes']);
    }
  }

}
