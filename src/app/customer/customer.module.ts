import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerNewComponent } from './customer-new/customer-new.component';

import { CustomerService } from './customer-service/customer.service';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    CustomerListComponent,
    CustomerNewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [
    CustomerService
  ]
})
export class CustomerModule { }
