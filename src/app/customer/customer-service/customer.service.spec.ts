import { TestBed } from '@angular/core/testing';
import { CustomerService } from './customer.service';
import { AngularFirestore } from '@angular/fire/firestore';

describe('CustomerService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerService,
        { provide: AngularFirestore, useValue: null },
      ],
    })
      .compileComponents();
  });

  it('should be created', () => {
    const service: CustomerService = TestBed.get(CustomerService);
    expect(service).toBeTruthy();
  });
});
