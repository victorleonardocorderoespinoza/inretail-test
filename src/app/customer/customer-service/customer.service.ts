import { Customer } from './Customer';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

@Injectable()
export class CustomerService {

  colection = 'customers';
  lifeExpectancy = 71.5;

  constructor(private firestore: AngularFirestore) { }

  get() {
    return this.firestore.collection(this.colection).snapshotChanges().pipe(
      map(data => data.map(
        item => {
          const age = moment().diff(item.payload.doc.get('dob'), 'years', false);
          const death = moment(item.payload.doc.get('dob')).add(this.lifeExpectancy, 'year');
          return { age, death, ...item.payload.doc.data() };
        })
      ));
  }

  add(customer: Customer) {
    this.firestore.collection(this.colection).add(customer);
  }
}
