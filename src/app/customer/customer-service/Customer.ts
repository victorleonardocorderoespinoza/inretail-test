export class Customer {
  id: string;
  firstName: string;
  lastName: string;
  age: number;
  dob: string;
}
