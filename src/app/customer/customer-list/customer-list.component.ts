import { CustomerService } from './../customer-service/customer.service';
import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer-service/Customer';

@Component({
  selector: 'in-customer-list',
  templateUrl: './customer-list.component.html'
})
export class CustomerListComponent implements OnInit {

  customers = [];
  info = {
    average: 0,
    deviation: 0
  };

  constructor(private customerService: CustomerService) { }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers() {
    this.customerService.get().subscribe(
      (data: Customer[]) => {
        this.customers = data;

        const ages = data.map(item => item.age);
        this.info.average = this.getAverage(ages);
        this.info.deviation = this.getStandarDeviation(ages);
      }
    );
  }

  getAverage(ages) {
    return ages.reduce((prev, cur) => prev + cur, 0) / ages.length;
  }

  getStandarDeviation(ages) {
    return Math.sqrt(ages.reduce((sq, n) => sq + Math.pow(n - this.info.average, 2), 0) / (ages.length - 1));
  }

}
