import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerListComponent } from './customer/customer-list/customer-list.component';
import { CustomerNewComponent } from './customer/customer-new/customer-new.component';
import { CustomerModule } from './customer/customer.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'clientes'
  },
  {
    path: 'clientes',
    children: [
      {
        path: '',
        component: CustomerListComponent
      },
      {
        path: 'nuevo',
        component: CustomerNewComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CustomerModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
